/**
 *  * itsc.commonui.js
 * @file Some common UI elements including dialog boxes, blocking UI
 *
 * @version 2.0.0
 * @author  Eric Dieckman
 * @updated 2018-05-23
 * @link    https://git.aae.wisc.edu/open-source/js.commonui
 * * 
 * Module that provides some common UI elements.
 * @module CommonUI
 * 
 * @requires jquery
 * @requires boostrap3-dialog
 * @requires block-ui
 */

var CommonUI = (function () {

    /* =================== private methods ================= */

    /* =================== public methods ================== */

    /**
     * Displays a delete confirmation dialog box.
     * @function
     * @param {string} title - Title displayed on the modal dialog
     * @param {string} message - Message to be displayed in the body of the modal dialog
     * @param {dialog_result} result_function - callback function
     * 
     */
    var delete_confirm_dialog = function (title, message, result_function) {
        return BootstrapDialog.show({
            type: BootstrapDialog.TYPE_DANGER,
            title: title,
            message: message,
            buttons: [{
                id: 'btn-del',
                icon: 'glyphicon glyphicon-trash',
                label: 'Delete',
                cssClass: 'btn-danger',
                action: function (dialogRef) {
                    dialogRef.close();
                    result_function(true);
                }
            },
            {
                id: 'btn-cancel',
                label: 'Cancel',
                cssClass: 'btn-default',
                action: function (dialogRef) {
                    dialogRef.close();
                    result_function(false);
                }
            }]
        });
    };

    /**
     * Blocks the UI with a message and loading animated gif
     * @method
     * @param {string} message
     * @static
     */
    var blockUI = function (message) {
        $.blockUI({
            message: '<h1>' + message + '</h1><img src="//api.aae.wisc.edu/img/ajax-loader-bar.gif" />',
            fadeIn: 0,
            fadeOut: 0
        });

    };

    /**
     * Unblocks the UI
     * @method
     * @static
     */
    var unblockUI = function () {
        $.unblockUI();
    };

    /**
     * 
     * Displays an alert confirmation dialog box.
     * @method
     * @param {string} title - Title displayed on the modal dialog
     * @param {string} message - Message to be displayed in the body of the modal dialog
     * @param {dialog_result} result_function - callback function
     * @static
     */
    var alert_dialog = function (title, message, result_function) {
        return BootstrapDialog.show({
            type: BootstrapDialog.TYPE_PRIMARY,
            title: title,
            message: message,
            buttons: [{
                id: 'btn-ok',
                icon: 'glyphicon glyphicon-ok',
                label: 'OK',
                cssClass: 'btn-primary',
                action: function (dialogRef) {
                    dialogRef.close();
                    result_function();
                }
            }]
        });
    };

    /**
    * 
    * Displays an error dialog box.
    * @method
    * @param {string} title - Title displayed on the modal dialog
    * @param {string} message - Message to be displayed in the body of the modal dialog
    * @param {dialog_result} result_function - callback function
    * @static
    */
    var error_dialog = function (title, message, result_function) {
        return BootstrapDialog.show({
            type: BootstrapDialog.TYPE_DANGER,
            size: BootstrapDialog.SIZE_WIDE,
            title: title,
            message: message,
            buttons: [{
                id: 'btn-ok',
                icon: 'glyphicon glyphicon-ok',
                label: 'OK',
                cssClass: 'btn-danger',
                action: function (dialogRef) {
                    dialogRef.close();
                    result_function();
                }
            }]
        });
    };

    /**
     * Displays a yes/no confirmation dialog box
     * @method
     * @param {string} title - Title displayed on the modal dialog
     * @param {string} message - Message to be displayed in the body of the modal dialog
     * @param {dialog_result} result_function - callback function
     * @static
     */
    var yesno_confirm_dialog = function (title, message, result_function) {
        return BootstrapDialog.show({
            type: BootstrapDialog.TYPE_PRIMARY,
            title: title,
            message: message,
            buttons: [{
                id: 'btn-yes',
                icon: 'glyphicon glyphicon-check',
                label: 'Yes',
                cssClass: 'btn-success',
                action: function (dialogRef) {
                    dialogRef.close();
                    result_function(true);
                }
            },
            {
                id: 'btn-no',
                label: 'No',
                icon: 'glyphicon glyphicon-ban-circle',
                cssClass: 'btn-warning',
                action: function (dialogRef) {
                    dialogRef.close();
                    result_function(false);
                }
            }]
        });
    };

    /* =============== export public methods =============== */

    return {
        /**
         * Callback for returning a result from a dialog box
         *
         * @callback dialog_result
         * @param {boolean} result - A boolean.
         * @static
         */

        /**
         * Displays a delete confirmation dialog box.
         * @function
         * @param {string} title - Title displayed on the modal dialog
         * @param {string} message - Message to be displayed in the body of the modal dialog
         * @param {dialog_result} result_function - callback function
         * @static
         * 
         */
        delete_confirm_dialog: delete_confirm_dialog,
        /**
         * Blocks the UI with a message and loading animated gif
         * @method
         * @param {string} message
         * @static
         */
        blockUI: blockUI,

        /**
         * Displays a yes/no confirmation dialog box
         * @method
         * @param {string} title - Title displayed on the modal dialog
         * @param {string} message - Message to be displayed in the body of the modal dialog
         * @param {dialog_result} result_function - callback function
         * @static
         */
        yesno_confirm_dialog: yesno_confirm_dialog,

        /**
         * 
         * Displays an error dialog box.
         * @method
         * @param {string} title - Title displayed on the modal dialog
         * @param {string} message - Message to be displayed in the body of the modal dialog
         * @param {dialog_result} result_function - callback function
         * @static
         */
        error_dialog: error_dialog,

        /**
         * 
         * Displays an alert confirmation dialog box.
         * @method
         * @param {string} title - Title displayed on the modal dialog
         * @param {string} message - Message to be displayed in the body of the modal dialog
         * @param {dialog_result} result_function - callback function
         * @static
         */
        alert_dialog: alert_dialog,

        /**
         * Unblocks the UI
         * @method
         */
        unblockUI: unblockUI


    };
}
    ());
